import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Pressable,
  StyleSheet,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { SafeAreaView } from "react-native-safe-area-context";
import { useNavigation } from "@react-navigation/native";
import { WebView } from "react-native-webview";



function HomeScreen() {
  const navigation = useNavigation();

  const names = [
    {
      name: "user1",
      id: 1,
    },
    {
      name: "user2",
      id: 2,
    },
    {
      name: "user3",
      id: 3,
    },
  ];

  const goToScreen = () => {
    navigation.navigate("HomeScreen2", {
     obj:names[0]
    });
  };

  return (
    <TouchableOpacity activeOpacity={0.9} onPress={goToScreen}>

    <View>
      <FlatList
        style={styles.list}
        data={names}
        keyExtractor= {name => name.name}
        renderItem={({item}) => 
          <Text> {item.name} - {item.id} </Text>}
      />
    </View>
     </TouchableOpacity>

  )

}
  

function HomeScreen1( {navigation, route} ) {
  const {obj}=route.params

  console.log(route);

  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text> {obj.name}</Text>
    </View>
  );
}


function HomeScreen2( {navigation, route} ) {
  const {obj}=route.params

  console.log(route);

  return (
    <View style={{ width: "100%", height: "100%" }}>
          <WebView
            source={{ uri: "https://google.com" }}
            onLoad={console.log("loaded")}
          ></WebView>
        </View>
  );
}


const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="HomeScreen1" component={HomeScreen1} />
        <Stack.Screen name="HomeScreen2" component={HomeScreen2} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}


const styles = StyleSheet.create({
  list: {
    marginTop: 100
  }
})

export default App;
